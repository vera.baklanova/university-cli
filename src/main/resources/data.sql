INSERT INTO lector(lector_first_name, lector_last_name, lector_degree, lector_salary)
VALUES('John', 'Hopkins', 2, 100.0 );
INSERT INTO lector(lector_first_name, lector_last_name, lector_degree, lector_salary)
VALUES('Barry', 'Bill', 0, 100.0 );
INSERT INTO lector(lector_first_name, lector_last_name, lector_degree, lector_salary)
VALUES('Samantha', 'James', 0, 100.0 );
INSERT INTO lector(lector_first_name, lector_last_name, lector_degree, lector_salary)
VALUES('John', 'Jacobson', 2, 150.0 );
INSERT INTO lector(lector_first_name, lector_last_name, lector_degree, lector_salary)
VALUES('Johnathan', 'Hopkins', 1, 50.0 );
INSERT INTO lector(lector_first_name, lector_last_name, lector_degree, lector_salary)
VALUES('Miranda', 'Hopkins', 1, 1000.0 );

INSERT INTO department(department_name) VALUES('Mathematics');
INSERT INTO department(department_name) VALUES('Roman Languages');

INSERT INTO department_head(department_id, head_lector_id) VALUES(1, 1);
INSERT INTO department_head(department_id, head_lector_id) VALUES(2, 4);

INSERT INTO lector_department(lector_id, department_id) VALUES(1, 1);
INSERT INTO lector_department(lector_id, department_id) VALUES(2, 1);
INSERT INTO lector_department(lector_id, department_id) VALUES(3, 1);
INSERT INTO lector_department(lector_id, department_id) VALUES(4, 1);

INSERT INTO lector_department(lector_id, department_id) VALUES(5, 2);
INSERT INTO lector_department(lector_id, department_id) VALUES(6, 2);
INSERT INTO lector_department(lector_id, department_id) VALUES(3, 2);
INSERT INTO lector_department(lector_id, department_id) VALUES(4, 2);