package com.example.springboot.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
public class Lector {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="lector_id")
    private Long id;

    @Column(name = "lector_first_name",nullable = false)
    @NonNull private String firstName;

    @Column(name="lector_last_name", nullable = false)
    @NonNull private String lastName;

    @Column(name="lector_degree", nullable = false)
    @NonNull private DegreeType degree;

    @Column(name="lector_salary", nullable = false)
    @NonNull private Double salary;

    @JsonBackReference
    @OneToOne(mappedBy = "head", fetch = FetchType.EAGER)
    @ToString.Exclude
    private Department headOfDepartment;

    @JsonBackReference
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "lector_department",
            joinColumns = @JoinColumn(name = "lector_id"),
            inverseJoinColumns = @JoinColumn(name = "department_id"))
    @ToString.Exclude
    private Collection<Department> departments;

    public String getFullName() { return firstName + " " + lastName; }
}
