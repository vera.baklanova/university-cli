package com.example.springboot.model.entity;

public enum DegreeType {
    ASSISTANT(0,"ASSISTANT"),
    ASSOCIATE(1, "ASSOCIATE PROFESSOR"),
    PROFESSOR(2, "PROFESSOR");

    private final int numValue;

    private final String name;

    @Override
    public String toString() { return name; }

    public int getNumValue() { return numValue; }

    private DegreeType(int n, String name) {
        this.numValue = n;
        this.name = name;
    }

}
