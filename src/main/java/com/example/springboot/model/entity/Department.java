package com.example.springboot.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "department_id")
    private Long id;

    @Column(name="department_name", nullable = false, unique = true)
    @NonNull private String name;

    @JsonManagedReference
    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "department_head",
            joinColumns =
                    { @JoinColumn(name = "department_id", referencedColumnName = "department_id") },
            inverseJoinColumns =
                    { @JoinColumn(name = "head_lector_id", referencedColumnName = "lector_id") })
    @NonNull private Lector head;

    @JsonManagedReference
    @ManyToMany(mappedBy = "departments", fetch = FetchType.EAGER)
    @ToString.Exclude
    @NonNull private Collection<Lector> lectors;

    public long professorsCount() {
        return lectors.stream()
                .filter(lector -> lector.getDegree() == DegreeType.PROFESSOR).count();
    }

    public long associatesCount() {
        return lectors.stream()
                .filter(lector -> lector.getDegree() == DegreeType.ASSOCIATE).count();
    }

    public long assistantsCount() {
        return lectors.stream()
                .filter(lector -> lector.getDegree() == DegreeType.ASSISTANT).count();
    }

    public double meanSalary() {
        return lectors.stream().mapToDouble(lector -> lector.getSalary()).sum() / lectors.size();
    }

    public long employeeCount() {
        return lectors.size();
    }

    public Lector getHeadLector() { return head; }

    public String departmentStats() {
        return String.format("%s department statistics.\n" +
                "\t\t\t\tassistants - %d.\n" +
                "\t\t\t\tassociate professors - %d\n" +
                "\t\t\t\tprofessors - %d", name, assistantsCount(), associatesCount(), professorsCount());
    }
}
