package com.example.springboot.service.repository;

import com.example.springboot.model.entity.Department;
import com.example.springboot.model.entity.Lector;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface LectorRepository extends CrudRepository<Lector, Long>, JpaSpecificationExecutor<Lector> {

    Optional<Lector> findById(final Long lectorId);
}
