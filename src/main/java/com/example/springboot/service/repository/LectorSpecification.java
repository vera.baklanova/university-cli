package com.example.springboot.service.repository;

import com.example.springboot.model.entity.Lector;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Expression;

public class LectorSpecification {
    public static Specification<Lector> fullNameContains(String searchWord) {
        return ((root, criteriaQuery, criteriaBuilder) -> {
            String regex = "%" + searchWord.toLowerCase() + "%";
            return criteriaBuilder.or(criteriaBuilder.like(criteriaBuilder.lower(root.get("firstName")), regex),
                    criteriaBuilder.like(criteriaBuilder.lower(root.get("lastName")), regex));
        });
    }
}
