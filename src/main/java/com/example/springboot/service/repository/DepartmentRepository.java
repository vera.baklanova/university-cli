package com.example.springboot.service.repository;

import com.example.springboot.model.entity.Department;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface DepartmentRepository extends CrudRepository<Department, Long> {
    Optional<Department> findByName(final String departmentName);

    Optional<Department> findById(final Long departmentId);
}
