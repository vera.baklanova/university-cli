package com.example.springboot.service.controller;

import com.example.springboot.model.entity.DegreeType;
import com.example.springboot.model.entity.Lector;
import com.example.springboot.service.repository.LectorRepository;
import com.example.springboot.service.repository.LectorSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/lector")
public class LectorController {
    @Autowired
    private LectorRepository repo;

    @GetMapping
    public Iterable<Lector> allLectors() { return repo.findAll(); }

    @GetMapping("/assistant")
    public Iterable<Lector> assistants() { return StreamSupport.stream(repo.findAll().spliterator(), false)
            .filter(lector -> lector.getDegree() == DegreeType.ASSISTANT).collect(Collectors.toList());
    }

    @GetMapping("/associate")
    public Iterable associates() { return StreamSupport.stream(repo.findAll().spliterator(), false)
            .filter(lector -> lector.getDegree() == DegreeType.ASSOCIATE).collect(Collectors.toList());
    }

    @GetMapping("/professor")
    public Iterable<Lector> professors() { return StreamSupport.stream(repo.findAll().spliterator(), false)
            .filter(lector -> lector.getDegree() == DegreeType.PROFESSOR).collect(Collectors.toList());
    }

    @GetMapping("/id/{lectorId}")
    public Optional<Lector> findById(@PathVariable final Long lectorId) {
        if (lectorId == null)
            return Optional.empty();
        return repo.findById(lectorId);
    }

    @GetMapping("/_search")
    public Iterable<Lector> findByName(@RequestParam String name) {
        Specification<Lector> fullNameSearch = LectorSpecification.fullNameContains(name);
        return repo.findAll(fullNameSearch);
    }
}
