package com.example.springboot.service.controller;

import com.example.springboot.model.entity.Department;
import com.example.springboot.service.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/department")
public class DepartmentController {
    @Autowired
    private DepartmentRepository repo;

    @GetMapping
    public Iterable<Department> allDepartments() {
        return repo.findAll();
    }

    @GetMapping("/id/{departmentId}")
    public Optional<Department> findById(@PathVariable final Long departmentId) {
        if (departmentId == null)
            return Optional.empty();
        return repo.findById(departmentId);
    }

    @GetMapping("/id/{departmentId}/stats")
    public String showDepartmentStats(@PathVariable final Long departmentId) {
        if (departmentId == null)
            return null;
        Optional<Department> d = repo.findById(departmentId);
        return d.map(Department::departmentStats).orElse(null);
    }

    public String showDepartmentStatsByDepartmentName(final String departmentName) {
        if (departmentName == null)
            return "Department not found";
        Optional<Department> d = repo.findByName(departmentName);
        if (d.isPresent())
            return d.get().departmentStats();
        return "Department not found";
    }

    public String meanSalaryByDepartmentName(final String departmentName) {
        if (departmentName == null)
            return "Department not found";
        Optional<Department> d = repo.findByName(departmentName);
        if (d.isPresent())
            return String.format("Mean salary for %s department is %10.3f$", departmentName, d.get().meanSalary());
        return "Department not found";
    }

    public Optional<Long> employeeCountByDepartmentName(final String departmentName) {
        if (departmentName == null)
            return Optional.empty();
        Optional<Department> d = repo.findByName(departmentName);
        return d.map(Department::employeeCount);
    }

    public String departmentHeadByDepartmentName(final String departmentName) {
        if (departmentName == null)
            return "Department not found";
        Optional<Department> d = repo.findByName(departmentName);
        if (d.isPresent())
            return String.format("Head of %s department is %s", departmentName, d.get().getHeadLector().getFullName());
        return "Department not found";
    }
}
