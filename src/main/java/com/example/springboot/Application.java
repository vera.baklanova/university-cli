package com.example.springboot;

import com.example.springboot.model.entity.Department;
import com.example.springboot.model.entity.Lector;
import com.example.springboot.service.controller.DepartmentController;
import com.example.springboot.service.controller.LectorController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@EntityScan("com.example.springboot.model.entity")
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner cliRunner(LectorController lectorController,
                                       DepartmentController departmentController) {
        return args -> {
            log.info("UNIVERSITY APPLICATION STARTED.");
            System.out.println("----------------------------------------------------");
            System.out.println("UNIVERSITY DEPARTMENTS");
            for (Department d : departmentController.allDepartments()) {
                System.out.println(d.departmentStats().toUpperCase());
                System.out.format("Mean salary: $%10.3f\n", d.meanSalary());
                System.out.println("----------------------------------------------------");
            }
            System.out.println("----------------------------------------------------");
            System.out.println("UNIVERSITY LECTORS");
            for (Lector l : lectorController.allLectors()) {
                System.out.println(l.toString());
            }
            System.out.println("----------------------------------------------------");

            System.out.println("HEAD OF DEPARTMENT");
            System.out.println(
                    departmentController.departmentHeadByDepartmentName("Mathematics")
            );
            System.out.println("----------------------------------------------------");


            System.out.println("DEPARTMENT STATS");
            System.out.println(departmentController.employeeCountByDepartmentName("Roman Languages"));
            System.out.println("----------------------------------------------------");


            System.out.println("MEAN SALARY");
            System.out.println(departmentController.meanSalaryByDepartmentName("Mathematics"));
            System.out.println("----------------------------------------------------");


            System.out.println("EMPLOYEE COUNT");
            System.out.println(departmentController.employeeCountByDepartmentName("Roman Languages").get());
            System.out.println("----------------------------------------------------");

            System.out.println("GLOBAL LECTOR SEARCH BY NAME PART");
            String lectorName = "hopkins";
            Iterable<Lector> lector = lectorController.findByName(lectorName);
            System.out.format("Found with query={%s}: %s\n", lectorName, lector.toString());
            System.out.format("Found with query={%s}: %s\n", "obi", lectorController.findByName("obi").toString());
            System.out.format("Found with query={%s}: %s\n", "ohn", lectorController.findByName("ohn").toString());
            System.out.println("----------------------------------------------------");
        };
    }


}
