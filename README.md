# University cli app

Works in console and web (api) mode, switch between `dev` and `web` profiles to test them both.

Implements the following commands:

1. Get head lector of the department by department name

2. Show department stats by department name

3. Show mean salary for the department by department name

4. Show employee count of the department by department name

5. Lector search by name part (ex. van -> Ivan Petrenko, Petro Ivanov)

Also provides a number of get request mappings for both department and lector entities.
````
/department
/department/id/{department_id}
/department/id/{department_id}/stats

/lector
/lector/id/{lector_id}
/lector/_search?name={fullNameSearchWord}
/lector/assistant
/lector/associate
/lector/professor
````